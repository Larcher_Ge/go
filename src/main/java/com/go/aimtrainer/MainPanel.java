package com.go.aimtrainer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MainPanel extends JPanel {

    private Logik l = new Logik(this);
    private JLabel score = new JLabel();



    public MainPanel(){
        setBackground(Color.gray);
        setSize(1000,1000);
        score.setBounds(450, 50 ,100,30);
        add(score);

        addMouseListener((new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                l.onClick(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        }));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        score.setText("Score: " + String.valueOf(l.getScore()));
        l.renderBalls(g);
    }
}
