package com.go.aimtrainer;

import javax.swing.*;
import java.util.jar.JarFile;

public class MainFrame extends JFrame {

    MainPanel mp = new MainPanel();

    public MainFrame(){
        setTitle("AimTariner");
        setSize(1000,1000);
        setDefaultCloseOperation(3);
        setLayout(null);
        setContentPane(mp);
        setVisible(true);

    }

}
