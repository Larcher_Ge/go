package com.go.aimtrainer;

import java.awt.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Ball {
    private int PosX, PosY, radius;
    
    private Color color;

    public Ball(int posX, int posY, int radius, Color color) {
        PosX = posX;
        PosY = posY;
        this.radius = radius;
        this.color = color;
    }

    public void decreaseSize(int startdelay, int disposetime){
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
        ses.scheduleAtFixedRate(()-> {
            radius--;

        }, startdelay, disposetime, TimeUnit.MILLISECONDS);
    }



    public int getPosX() {
        return PosX;
    }

    public void setPosX(int posX) {
        PosX = posX;
    }

    public int getPosY() {
        return PosY;
    }

    public void setPosY(int posY) {
        PosY = posY;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
