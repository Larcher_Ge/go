package com.go.aimtrainer;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Logik {

    private MainPanel mp;

    private int score;
    
    private ArrayList<Ball> balls = new ArrayList<>();
    public Logik(MainPanel mp){
        this.mp = mp;
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
        ses.scheduleAtFixedRate(()-> mp.updateUI(), 0,10, TimeUnit.MILLISECONDS);
        createBall();
    }

    public void createBall(){
        Random rdm = new Random();
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
        ses.scheduleAtFixedRate(() -> {
            Ball b = new Ball(rdm.nextInt(900), rdm.nextInt(900),150,Color.red);

            balls.add(b);
            b.decreaseSize(100,20);

        },100,700, TimeUnit.MILLISECONDS);

    }

    public void renderBalls(Graphics g){
        for(Ball b : balls) {
            if(b.getRadius() > 45) {
                g.setColor(b.getColor());
                g.fillOval(b.getPosX(), b.getPosY(), (int) b.getRadius(), (int) b.getRadius());
                g.setColor(Color.black);
            }else balls.remove(b);
        }
    }

    public void onClick(MouseEvent e){
        for (Ball b :balls) {
            if(e.getX() > b.getPosX() && b.getPosX() + b.getRadius() > e.getX()&& e.getY() > b.getPosY() &&  b.getPosY() + b.getRadius() > e.getY()){
                balls.remove(b);
                score++;
            }
        }
    }
    
    public int getScore() {
        return score;
    }



}
