package com.go.Interfaces.bo;

public class Auto implements Ifahrzeug {
    int kilometerstand;
    int bauJahr;
    int anzahlTüren;

    public Auto(int kilometerstand, int bauJahr, int anzahlTüren){
        this.kilometerstand = kilometerstand;
        this.bauJahr = bauJahr;
        this.anzahlTüren = anzahlTüren;
    }

    boolean gebautBevorJahr(int Jahr) {
        return this.bauJahr < Jahr;
    }

    @Override
    public double maschieneStarten() {
        return 0;
    }

    @Override
    public boolean anzahlPassagiere() {
        return false;
    }
}
