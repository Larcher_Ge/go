package com.go.Interfaces.bo;

import javax.swing.*;

public interface Ifahrzeug  {
    public double maschieneStarten();
    public boolean anzahlPassagiere();
}
