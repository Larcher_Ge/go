package com.go.Interfaces.bo;

public class Motorad implements Ifahrzeug{
    int kilometerstand;
    int anzahlRaeder;

    public Motorad(int kilometerstand,int anzahlRaeder){
        this.kilometerstand = kilometerstand;
        this.anzahlRaeder = anzahlRaeder;
    }

    @Override
    public double maschieneStarten() {
        return 0;
    }

    @Override
    public boolean anzahlPassagiere() {
        return false;
    }
}
