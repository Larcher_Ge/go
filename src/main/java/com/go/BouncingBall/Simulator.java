package com.go.BouncingBall;

import javax.swing.*;

public class Simulator extends JFrame {


    private static int box_height = 1400;
    private static int box_width = 1400;

    private static int updateRate = 40;

    private static Gui gui;

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(box_width,box_height);
        frame.setLocationRelativeTo(null);

        gui = new Gui();
        frame.setContentPane(gui);
        frame.setVisible(true);

    }

    public static int getBox_height() {
        return box_height;
    }

    public  static  int getBox_width() {
        return box_width;
    }

    public static Gui getGui() {
        return gui;
    }

    public static int getUpdateRate() {
        return updateRate;
    }
}
