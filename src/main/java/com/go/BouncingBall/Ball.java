package com.go.BouncingBall;

import java.awt.*;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class Ball {

    private int box_height = Simulator.getBox_height();
    private int box_width = Simulator.getBox_width();

    private int radius = 20;

    private int ball_speed_x = 10;
    private int ball_speed_y = 15;

    private int updateRate = Simulator.getUpdateRate();

    private int ball_x = 120;
    private int ball_y = 20;

    private Color color;

    public Ball(int radius, int ball_speed_x, int ball_speed_y, int ball_x, int ball_y, Color color) {
        this.radius = radius;
        this.ball_speed_x = ball_speed_x;
        this.ball_speed_y = ball_speed_y;
        this.ball_x = ball_x;
        this.ball_y = ball_y;
        this.color = color;
    }

    public void startBall(){
        while(true){
            ball_x+= ball_speed_x;
            ball_y+= ball_speed_y;

            // X-Achse
            if(ball_x + radius > box_width) {
                ball_speed_x = -ball_speed_x;
                ball_x = box_width-radius;
            } if(ball_x  <= 0) {
                ball_speed_x = -ball_speed_x;
                ball_x = 0;
            }

            // Y-ACHSE

            if(ball_y + radius > box_height) {
                ball_speed_y = -ball_speed_y;
                ball_y = box_height-radius;


            }  if(ball_y <= 0) {
                ball_speed_y = -ball_speed_y;
                ball_y = 0;
            }
            Simulator.getGui().repaint();

            try {
                Thread.sleep(Simulator.getUpdateRate());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void runBall(){

        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                startBall();
            }
        });
        th.start();
    }



    public int getRadius() {
        return radius;
    }

    public int getBall_speed_x() {
        return ball_speed_x;
    }

    public int getBall_speed_y() {
        return ball_speed_y;
    }

    public int getUpdateRate() {
        return updateRate;
    }

    public int getBall_x() {
        return ball_x;
    }

    public int getBall_y() {
        return ball_y;
    }

    public Color getColor() {
        return color;
    }
}
