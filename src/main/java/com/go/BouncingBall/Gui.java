package com.go.BouncingBall;



import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;


public class Gui extends JPanel {

    private ArrayList<Ball> b = new ArrayList<>();

    private Graphics g;






    public Gui() {
        setPreferredSize(new Dimension(Simulator.getBox_width(), Simulator.getBox_height()));
        b.add(new Ball(10, 10,15,0,10, Color.green));
        b.add(new Ball(10, 20,10,10,10, Color.red));
        b.add(new Ball(10, 15,10,15,25, Color.magenta));
        b.add(new Ball(15, 25,5,40,10, Color.cyan));
        b.add(new Ball(7, 10,15,0,10, Color.BLUE));
    }



    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        this.g = g;
        g.setColor(Color.black);
        g.fillRect(0,0,Simulator.getBox_width(), Simulator.getBox_height());

        for(Ball ball : b) {
            g.setColor(ball.getColor());
            g.fillOval(ball.getBall_x(), ball.getBall_y(), ball.getRadius(), ball.getRadius());
            ball.runBall();
        }


    }


}
