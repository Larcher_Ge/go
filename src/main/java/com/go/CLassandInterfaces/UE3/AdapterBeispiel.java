package com.go.CLassandInterfaces.UE3;

public class AdapterBeispiel {

    public static void main(String[] args) {
        new MacherInterAdapter() {
            @Override
            public void machEtzwas() {
                System.out.println("Mach was mit dem Adapter");
            }
        }.machEtzwas();
    }
}
