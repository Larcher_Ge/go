package com.go.pong;

import java.awt.event.KeyEvent;

public class Balken {

    private int box_height = Simulator.getBox_height();
    private int box_width = Simulator.getBox_width();

    private int width = 10;
    private int height = 100;



    private int key_up,key_down;


    private int balken_x;
    private int balken_y;


    public Balken( int ball_x, int ball_y, int key_up, int key_down) {

        this.balken_x = ball_x;
        this.balken_y = ball_y;
        this.key_up = key_up;
        this.key_down = key_down;

    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getBalken_x() {
        return balken_x;
    }

    public int getBalken_y() {
        return balken_y;
    }

    public void setBalken_y(int balken_y) {
        this.balken_y = balken_y;
    }
}

