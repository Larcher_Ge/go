package com.go.pong;

import javax.swing.*;

public class Simulator extends JFrame {


    private static int box_height = 1250;
    private static int box_width = 1100;

    private static int updateRate = 40;

    private static Logik l = new Logik();
    private static MainPanel gui = new MainPanel(l);


    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(box_width,box_height);
        frame.setLocationRelativeTo(null);
        frame.addKeyListener(gui);

        frame.setContentPane(gui);
        frame.setVisible(true);

    }

    public static int getBox_height() {
        return box_height;
    }

    public  static  int getBox_width() {
        return box_width;
    }

    public static MainPanel getGui() {
        return gui;
    }

    public static Logik getL() {
        return l;
    }

    public static int getUpdateRate() {
        return updateRate;
    }
}
