package com.go.pong;

import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Logik {
    private int scorePlayerLeft= 0, scorePlayerRight = 0;

    private Ball ball;
    private Balken balken_left;
    private Balken balken_rigth;
    private Random rdm = new Random();
    public Logik(){

    }

    public void createObjekts() {
        ball = new Ball(10,15,15,500,500);
        ball.runBall();
        balken_left = new Balken(50,0, KeyEvent.VK_UP, KeyEvent.VK_DOWN);
         balken_rigth = new Balken(1040,0, KeyEvent.VK_W, KeyEvent.VK_S);
         ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
         ses.scheduleAtFixedRate(() -> {
             boolean d1 = rdm.nextBoolean();
             boolean d2 = rdm.nextBoolean();
             int bsx = 0;
             int bsy = 0;
             if(d1)  bsx = -15;
             if(d2)  bsy = -15;
             if(!d1)  bsx = 15;
             if(!d2)  bsy = 15;

             if(ball.getBall_x() < 20 ) {
                 scorePlayerRight++;

                 ball = new Ball(10,bsx,bsy,500,500);
                 ball.runBall();
             }
             if(ball.getBall_x() > 1080 ) {
                 scorePlayerLeft++;
                 ball = new Ball(10,bsx,bsy,500,500);
                 ball.runBall();
             }
         },0,10, TimeUnit.MILLISECONDS);
    }

    public void onKeyPress() {
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
        ses.scheduleAtFixedRate(()-> {
            if( balken_left.getBalken_y() > 0 && Simulator.getGui().isW()) balken_left.setBalken_y(balken_left.getBalken_y() - 15);
            if(balken_left.getBalken_y() < 1200- balken_left.getHeight()&&Simulator.getGui().isA()) balken_left.setBalken_y(balken_left.getBalken_y()+15);
            if(balken_rigth.getBalken_y() > 0&&Simulator.getGui().isS()) balken_rigth.setBalken_y(balken_rigth.getBalken_y()-15);
            if(balken_rigth.getBalken_y() < 1200- balken_rigth.getHeight()&&Simulator.getGui().isD()) balken_rigth.setBalken_y(balken_rigth.getBalken_y()+15);


        },0,20, TimeUnit.MILLISECONDS);
//        switch (e.getKeyCode()){
//            case KeyEvent.VK_W -> {
//                if(balken_left.getBalken_y() > 0) {
//                    balken_left.setBalken_y(balken_left.getBalken_y() - 15);
//                }
//            }
//            case KeyEvent.VK_S ->{
//                if(balken_left.getBalken_y() < 1200- balken_left.getHeight()) {
//                    balken_left.setBalken_y(balken_left.getBalken_y()+15);
//                }
//            }
//
//            case KeyEvent.VK_UP -> {
//                if(balken_rigth.getBalken_y() > 0) {
//                    balken_rigth.setBalken_y(balken_rigth.getBalken_y()-15);
//
//                }
//            }
//            case KeyEvent.VK_DOWN -> {
//                if(balken_rigth.getBalken_y() < 1200- balken_rigth.getHeight()) {
//                    balken_rigth.setBalken_y(balken_rigth.getBalken_y()+15);
//                }
//
//            }
//
//
//        }
    }


    public Ball getBall() {
        return ball;
    }

    public int getScorePlayerLeft() {
        return scorePlayerLeft;
    }
    public int getScorePlayerRight() {
        return scorePlayerRight;
    }

    public Balken getBalken_left() {
        return balken_left;
    }

    public Balken getBalken_rigth() {
        return balken_rigth;
    }
}
