package com.go.pong;



import java.awt.*;

public class Ball {
    private int box_height = Simulator.getBox_height()-50;
    private int box_width = Simulator.getBox_width();

    private int radius = 20;

    private int ball_speed_x = 10;
    private int ball_speed_y = 15;


    private int ball_x = 120;
    private int ball_y = 20;


    public Ball(int radius, int ball_speed_x, int ball_speed_y, int ball_x, int ball_y) {
        this.radius = radius;
        this.ball_speed_x = ball_speed_x;
        this.ball_speed_y = ball_speed_y;
        this.ball_x = ball_x;
        this.ball_y = ball_y;

    }

    public void startBall(){
        while(true){
            ball_x+= ball_speed_x;
            ball_y+= ball_speed_y;
            // checkt id hit x
//            if(ball_x + radius > box_width) {
//                ball_speed_x = -ball_speed_x;
//
//            }

            if(ball_x  >= 1050&& ball_y > Simulator.getL().getBalken_rigth().getBalken_y() && ball_y < Simulator.getL().getBalken_rigth().getBalken_y()+Simulator.getL().getBalken_rigth().getHeight()){
                ball_speed_x = -ball_speed_x;

            }
            if(ball_x  <= 50&& ball_y > Simulator.getL().getBalken_left().getBalken_y() && ball_y < Simulator.getL().getBalken_left().getBalken_y()+Simulator.getL().getBalken_left().getHeight()){
                ball_speed_x = -ball_speed_x;

            }

            // X-Achse
//            if(ball_x + radius > box_width) {
//                ball_speed_x = -ball_speed_x;
//                ball_x = box_width-radius;
//            } if(ball_x  <= 0) {
//                ball_speed_x = -ball_speed_x;
//                ball_x = 0;
//            }

            // Y-ACHSE

            if(ball_y + radius > box_height) {
                ball_speed_y = -ball_speed_y;
                ball_y = box_height-radius;


            }  if(ball_y <= 0) {
                ball_speed_y = -ball_speed_y;
                ball_y = 0;
            }
            Simulator.getGui().repaint();

            try {
                Thread.sleep(Simulator.getUpdateRate());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void runBall(){

        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                startBall();
            }
        });
        th.start();
    }



    public int getRadius() {
        return radius;
    }

    public int getBall_speed_x() {
        return ball_speed_x;
    }

    public int getBall_speed_y() {
        return ball_speed_y;
    }



    public int getBall_x() {
        return ball_x;
    }

    public int getBall_y() {
        return ball_y;
    }


}
