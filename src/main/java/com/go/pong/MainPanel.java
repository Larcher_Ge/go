package com.go.pong;



import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MainPanel extends JPanel implements KeyListener {

    private Graphics g;
    private Ball ball;
    private Balken balken_left;
    private Balken balken_rigth;
    private Logik l;


    public MainPanel(Logik l){
        this.l = l;
        setPreferredSize(new Dimension(Simulator.getBox_width(), Simulator.getBox_height()));
        setBackground(Color.black);
        l.createObjekts();
        ball = l.getBall();
        balken_left = l.getBalken_left();
        balken_rigth = l.getBalken_rigth();


        updateFrame();
        l.onKeyPress();
    }

    public void updateFrame() {
        Thread updater = new Thread(()-> {
            while (true) {
                updateUI();
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        updater.start();
        
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        this.g = g;
        g.drawLine(549,0,549,1200);
        g.drawLine(550,0,550,1200);
        g.drawLine(551,0,551,1200);
        g.setColor(Color.white);
        g.setFont(new Font("Consolas",0,100));
        g.drawString(String.valueOf(l.getScorePlayerLeft()), 230, 100);
        g.drawString(String.valueOf(l.getScorePlayerRight()), 780, 100);
        g.setColor(Color.white);
        ball = l.getBall();
        g.fillOval(ball.getBall_x(), ball.getBall_y(), ball.getRadius(),ball.getRadius());
        g.fillRect(balken_left.getBalken_x(), balken_left.getBalken_y(),  balken_left.getWidth(),  balken_left.getHeight());
        g.fillRect(balken_rigth.getBalken_x(), balken_rigth.getBalken_y(),  balken_rigth.getWidth(),  balken_rigth.getHeight());



    }

    public Graphics getG() {
        return g;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    private boolean w = false, s = false, a = false, d = false;

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_W -> {
                w = true;
            }
            case KeyEvent.VK_S ->{
                a = true;
            }

            case KeyEvent.VK_UP -> {
                s = true;
            }
            case KeyEvent.VK_DOWN -> {
                d = true;
            }

        }


    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_W -> {
               w = false;
            }
            case KeyEvent.VK_S ->{
               a = false;
            }

            case KeyEvent.VK_UP -> {
                s = false;
            }
            case KeyEvent.VK_DOWN -> {
                d = false;
            }

        }
    }

    public boolean isW() {
        return w;
    }

    public boolean isS() {
        return s;
    }

    public boolean isA() {
        return a;
    }

    public boolean isD() {
        return d;
    }
}
