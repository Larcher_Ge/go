package com.go.jojo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;


public class bowertschonpassen {
    static JPanel p;
    static Color front_color;
    static int punkte;
    static JLabel l = new JLabel("");
    public static void main(String[] args) {
        JFrame f = new JFrame("Reaktionstest");

        f.setSize(1000,1000);
        f.setLayout(null);
        f.setDefaultCloseOperation(3);
        p = new JPanel();
        p.setSize(1000,1000);
        p.setLayout(null);
        punkteScore  = new JLabel("Punkte: 0");
        punkteScore.setBounds(100,100,100,30);

        p.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

                switch (e.getKeyChar()) {

                    case 'q' -> checkColor(Color.red);
                    case 'w'-> checkColor(Color.yellow);
                    case 'e' -> checkColor(Color.blue);
                    case 'r' -> checkColor(Color.green);

                }
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }

        });

        f.setContentPane(p);
        f.setVisible(true);
        p.add(punkteScore);
        l.setBounds(450,450,100,100);
        p.add(l);

        addbuttons();
        p.requestFocus();
        changeColors();
    }

    public static void addbuttons(){
        JButton b_rot= new JButton("Rot(Q)");
        JButton b_gelb= new JButton("Gelb(W)");
        JButton b_blau= new JButton("Blau(E)");
        JButton b_gruen= new JButton("Gruen(R)");

        b_rot.setBounds(300,700,100,100);
        b_gelb.setBounds(400,700,100,100);
        b_blau.setBounds(500,700,100,100);
        b_gruen.setBounds(600,700,100,100);

        p.add(b_rot);
        p.add(b_gelb);
        p.add(b_blau);
        p.add(b_gruen);

        b_rot.addActionListener((l)-> {
            checkColor(getColorbyString(b_rot.getText()));
        } );

        b_gelb.addActionListener((l)-> {
            checkColor(getColorbyString(b_gelb.getText()));
        } );

        b_blau.addActionListener((l)-> {
            checkColor(getColorbyString(b_blau.getText()));
        } );

        b_gruen.addActionListener((l)-> {
            checkColor(getColorbyString(b_gruen.getText()));
        } );

    }
    static JLabel punkteScore;
    public static void punkteSys(){
        punkteScore.setText("Punkte: " + punkte);
    }

    static int cd = 0;
    public static void checkColor(Color c){

        if(cd == 0) {
            if(front_color.equals(c)){
                punkte++;
                punkteSys();
            }
        }
        cd = 1;

    }

    public static Color getColorbyString(String color) {
        Color c = null;
        switch (color) {
            case "Rot" -> c = Color.red;
            case "Gelb" -> c = Color.yellow;
            case "Blau" -> c = Color.blue;
            case "Gruen" -> c = Color.green;
        }
        return c;
    }

    public static String getStringbyColor(Color co) {
        String c = null;
        if (Color.red.equals(co)) {
            c = "Rot";
        } else if (Color.yellow.equals(co)) {
            c = "Gelb";
        } else if (Color.blue.equals(co)) {
            c = "Blau";
        } else if (Color.green.equals(co)) {
            c = "Gruen";
        }
        return c;
    }

    static int counter = 0;
    public static void changeColors(){
        Color colors[] = new Color[]{Color.red, Color.yellow, Color.blue, Color.green};
        Random rdm = new Random();
        Thread t = new Thread(()-> {
            while(true) {
                Color bk = colors[rdm.nextInt(4)];
                Color fg = colors[rdm.nextInt(4)];
                while(bk.equals( fg)) {
                    fg = colors[rdm.nextInt(4)];
                }
                front_color = fg;
                l.setForeground(fg);
                l.setText(getStringbyColor(fg));
                p.setBackground(bk);
                cd =0;
                if(counter > 60) {
                    JOptionPane.showMessageDialog(null,"Gewonnen mit " + punkte + "Punkten");
                    Thread.currentThread().stop();

                }
                counter++;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });
        t.start();
    }


}
